package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        String one, two, three, oneWord, twoWord, threeWord;

        System.out.println("Type any sentence please:");
        one = scan.nextLine();
        System.out.println("\nType second sentence here:");
        two = scan.nextLine();
        System.out.println("\nType the last one:");
        three = scan.nextLine();

        if (one.contains("cat")) {
            System.out.println("\nI found: " + one);
        }
        if (two.contains("cat")) {
            System.out.println("\nAnd: " + two);
        }
        if (three.contains("cat")) {
            System.out.println("\nAnd again: " + three);
        } else {
            System.out.println("\nPlease try again.");
        }
    }
}
